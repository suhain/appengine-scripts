from datetime import datetime, timedelta
from google.cloud import datastore

client = datastore.Client(project='blitztinyarmypanoramic')

query = client.query(kind='ChannelMessage')
query.add_filter('posted_at', '>', datetime.utcnow() - timedelta(days=7))

fetch_size, cursor, more = 250, None, True


while more:
    query_iter = query.fetch(fetch_size, start_cursor=cursor)
    entities = list(query_iter)
    more = len(entities) == fetch_size
    cursor = query_iter.next_page_token

    to_put = []
    for m in entities:
        print m
        m.key = client.key("GuildMessage", m.key.id_or_name)
        print m
        to_put.append(m)
    client.put_multi(to_put)