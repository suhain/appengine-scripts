import json

from datetime import datetime, timedelta
from operator import attrgetter

from google.cloud import datastore


datastore_client = datastore.Client(project='blitztinyarmypanoramic')
day = 12
query = datastore_client.query(kind='PlayerDaysInGame')
query.add_filter('as_main', '>', day)
cursor, more, fetch_size = None, True, 150
count = 0
april = datetime(2018, 04, 03, tzinfo=None)
while more:
    query_iter = query.fetch(fetch_size, start_cursor=cursor)
    entities = list(query_iter)
    cursor = query_iter.next_page_token
    more = len(entities) == fetch_size
    to_put = []
    for e in entities:
        if e['bonus_generated_when'].replace(tzinfo=None) < april:
            continue
        print e
        e['as_main'] = day
        # print e.key.to_legacy_urlsafe()
        to_put.append(e)
        # break
    datastore_client.put_multi(to_put)
    # break
