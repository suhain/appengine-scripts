from datetime import datetime

from google.cloud import datastore


datastore_client = datastore.Client(project='xxx-mightyparty')
query = datastore_client.query(kind='EmailAuthAccount')
this_monday = datetime(2017, 11, 27, 18).replace(tzinfo=None)
fetch_size, cursor, more = 10, None, True

pay_signup = 0
new_pay_signup = 0

while more:
    query_iter = query.fetch(fetch_size, start_cursor=cursor)
    _entities = list(query_iter)
    cursor = query_iter.next_page_token
    more = fetch_size == len(_entities)
    login_ids = [e['profile_id'] for e in _entities]
    profile_ids = [
        parent_profile['parent_id']
        for parent_profile in datastore_client.get_multi([
            datastore_client.key("ParentProfile", login_id)
            for login_id in login_ids
        ])
    ]
    print profile_ids
    payments_info_bits = {
        bit.key.name: bit
        for bit in datastore_client.get_multi([
            datastore_client.key("PaymentsInfoBit", profile_id)
            for profile_id in profile_ids
        ])
    }
    print payments_info_bits
    player_profiles = {
        bit.key.name: bit
        for bit in datastore_client.get_multi([
            datastore_client.key("PlayerProfile", profile_id)
            for profile_id in profile_ids
        ])
    }
    print player_profiles
    for profile_id in profile_ids:
        if profile_id not in payments_info_bits.keys():
            continue
        if profile_id not in player_profiles.keys():
            continue
        payments_info_bit = payments_info_bits[profile_id]
        player_profile = player_profiles[profile_id]
        if player_profile['is_cheater'] or player_profile['is_qa_account']:
            continue
        if payments_info_bit['PaymentsCount'] > 1:
            pay_signup += 1
            if player_profile['created_datetime'].replace(tzinfo=None) >= this_monday:
                new_pay_signup += 1

print pay_signup, new_pay_signup
