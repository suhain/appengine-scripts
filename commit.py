# import json
# from pprint import pprint
#
# import requests
#
#
# session = requests.Session()
#
# session.headers.update({
#     'pnk-player-id': 'buhain',
#     'pnk-secure-string': 'skip',
#     'pnk-version': 'skip',
#     'pnk-env': 'LinuxEditor',
#     'pnk-device-id': 'archlinux',
#     'pnk-request-client-start-time': '1',
# })
#
# with open('commit_data.json') as f:
#     r = session.post(
#         'http://127.0.0.1:5000/gs_api/profile/commit',
#         json={
#             'data': json.loads(f.read()),
#             'revision': -2016,
#             'pvp_active': True,
#         }
#     )
#     print r.status_code, r.text
#
# r = session.post(
#     'http://127.0.0.1:5000/gs_api/profile/load',
#     json={}
# )
#
# print r.status_code
# pprint(r.json())
#
# # r = session.post(
# #     'http://127.0.0.1:5000/gs_api/pvp/get_opponent',
# #     json={
# #         'type': 'pvp',
# #     }
# # )
# #
# # print r.status_code, r.json()


def gen(name):
    print '%s started' % name
    yield
    print '%s entered some stage' % name
    yield
    print '%s exited' % name


gen1 = gen(1)
gen2 = gen(2)

next(gen1)
next(gen2)
next(gen1)
next(gen2)
try:
    next(gen1)
except StopIteration:
    pass
try:
    next(gen2)
except StopIteration:
    pass

# for _, _ in zip(gen1, gen2):
#     pass