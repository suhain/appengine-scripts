from datetime import datetime, timedelta

from google.cloud import datastore

datastore_client = datastore.Client(project='blitztinyarmypanoramic')
since = datetime.utcnow() - timedelta(days=30)


more, fetch_size, cursor = True, 500, None

profile_ids = []

query = datastore_client.query(kind='PlayerProfile')
query.add_filter('update_datetime', '>=', since)

while more:
    query_iter = query.fetch(fetch_size, start_cursor=cursor)
    profiles = list(query_iter)
    cursor = query_iter.next_page_token
    more = len(profiles) == fetch_size

    for profile in profiles:
        if profile.key.name.startswith('AND'):
            if profile.key.name not in profile_ids:
                profile_ids.append(profile.key.name)
    print profile_ids


with open('android_ids.txt', 'w') as f:
    f.write('\n'.join(profile_ids))