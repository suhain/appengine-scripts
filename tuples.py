from operator import itemgetter


class Dict(dict):
    a = property(itemgetter('a'))
    b = property(itemgetter('b'))


d = Dict({'a': 1, 'b': 2})
print d.a, d.b


class D(dict):
    __setattr__ = dict.__setitem__
    __getattr__ = dict.__getitem__
    __missing__ = lambda *a: None


d = D()
d.a = 1
d.a += 123
d['b'] = d.a
print d.a, d['a'], d.c, d.keys()
