from google.cloud import datastore


client = datastore.Client(project='blitztinyarmypanoramic')
# query = client.query(kind='GlobalChannelMessage')
# query = client.query(kind='GuildRoomMessage')
query = client.query(kind='ChannelMessage')
fetch_size, more, cursor = 500, True, None

while more:
    query_iter = query.fetch(fetch_size, start_cursor=cursor)
    messages = list(query_iter)
    cursor = query_iter.next_page_token
    more = len(messages) == fetch_size

    client.delete_multi([
        m.key for m in messages
    ])
    print len(messages)