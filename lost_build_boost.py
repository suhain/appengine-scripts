import json
import time

import random
from collections import defaultdict

from dateutil import parser
from google.cloud import logging, datastore

logging_client = logging.Client(project='blitztinyarmypanoramic', _use_grpc=False)
datastore_client = datastore.Client(project='blitztinyarmypanoramic')


filter_ = """
resource.type="gae_app"
resource.labels.module_id="default"
resource.labels.version_id="production"
logName="projects/blitztinyarmypanoramic/logs/appengine.googleapis.com%2Frequest_log"
protoPayload.resource="/gs_api/profile/commit"
timestamp>="2018-01-21T00:00:00Z"
"GuildShopBit"
"""
print filter_
data = defaultdict(dict)


def parse_pnk_headers(logMessage):
    headers = {}
    for header in logMessage.split('\n'):
        header_name, header_value = header.split(': ')
        header_name = header_name.strip()
        header_value = header_value.strip()
        headers[header_name] = header_value
    return headers


def get_headers(payload):
    try:
        for l in payload['line']:
            logMessage = l['logMessage']
            try:
                return parse_pnk_headers(logMessage)
            except (KeyError, ValueError):
                pass
            except Exception as e:
                print type(e), e
            else:
                continue
    except KeyError:
        pass


def parse_guild_shop_data(payload):
    for l in payload['line']:
        logMessage = l['logMessage']
        # print logMessage
        if logMessage.startswith('"GuildShopBit"'):
            s = logMessage.replace('"GuildShopBit": ', '')
            s = s.replace('u', '')
            s = s.replace("'", '"')
            s = s.replace('Tre', 'true')
            s = s.replace('True', 'true')
            start = s.find('"selectedBonses": ')
            if start == -1:
                return
            start += len('"selectedBonses": ')
            end, depth, ok = start, 1, False
            for i in range(start + 1, len(s)):
                if s[i] == '{':
                    depth += 1
                if s[i] == '}':
                    depth -= 1
                end = i
                if depth == 0:
                    ok = True
                    break
            if ok:
                return json.loads(s[start:end + 1])


def get_actions(payload):
    for l in payload['line']:
        logMessage = l['logMessage']
        # print logMessage
        if logMessage.startswith('Actions:'):
            return logMessage


total_losts = set()
for entry in logging_client.list_entries(filter_=filter_):

    headers = get_headers(entry.payload)
    if headers is None or 'Pnk-Player-Id' not in headers.keys():
        continue
    ts = entry.timestamp.replace(tzinfo=None)
    profile_id = headers['Pnk-Player-Id']

    selectedBonuses = parse_guild_shop_data(entry.payload)
    if selectedBonuses is None:
        continue
    tmp = {}
    for bonus_id, bonus_data in selectedBonuses.items():
        tmp[bonus_id] = parser.parse(bonus_data['endTime'])
    selectedBonuses = tmp
    actions = get_actions(entry.payload)
    if 'ClearGuildShopBonuses' in actions:
        data[profile_id] = {}
    # print ts, selectedBonuses, actions
    current_bonuses = data[profile_id]
    for bonus_id, bonus_endTime in current_bonuses.items():
        if bonus_id not in selectedBonuses.keys() and bonus_endTime > ts:
            print profile_id, "lost_bonus", bonus_id, bonus_endTime, ts, actions
            total_losts.add(profile_id)
    data[profile_id].update(**selectedBonuses)

    if random.randint(0, 3) == 0:
        time.sleep(random.randint(0, 1))
        print ts


for profile_id in total_losts:
    print profile_id
print len(total_losts)