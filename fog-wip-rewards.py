import requests
from datetime import datetime, timedelta
from google.cloud import datastore

# client = datastore.Client(project='tinyarmypanoramic')
# SEND_REWARDS_URL = 'http://dev.tinyarmypanoramic.appspot.com/_admin/rewards/custom_rewards'
# HEADERS = {
#     'COOKIE': 'ACSID=~AJKiYcERyDVAaD8UtnAlLWo3V0QEPeCRPz9cPB_m_TD_ogfH5DdOilqQUYZT0cXNBk7-d6KvMyuSN31M8VbnW4sLA2_kE31NwjY9Za1Kxxsv59evttmvLLISx3X2RVCKfTux84cysSYwlhjG_D5UBO8OQKEsGyXLqjWWShPbnYrgfnI6_DGe9egRzORXg1Q3vUJBze0vBYmQK5FbbBBsu4X4PZGkFENlib2jozhA-wNbYDf4_jPwXXMfrKdOdERlTUpgqsH9BG_6mJmhckeJmctwzfaxhr7_k8yYToqpttlIUcACYuPVgj70HDRqgiAgRzj2hvsvkjBCT0NRc5vmbiHj7UBbwUoLdQ'
# }
client = datastore.Client(project='steamtinyarmypanoramic')
SEND_REWARDS_URL = 'http://dev.steamtinyarmypanoramic.appspot.com/_admin/rewards/custom_rewards'
HEADERS = {
    'COOKIE': 'ACSID=~AJKiYcEpaayU7fI8J35zztPKWiNYEZBK0_0Vwm1Xci_fQZBcVFwbJpkpTppT6Bhw8xMRiWYKKoHZRRYqfPs14spobKVu23vEmf1z-dIki_hWaparzvm-1rnKELgu6rqkVAdMR_T2FKjpsiYjHE-iTBIhtq7GM_7TMHnwmPYHwPM3ewshIS8TWV3H6lHNE36o3f63-9-wUsFzxPd389YMWLyyQrmjc4qLeAcrKIR1GXTZ_Xe9WtIYLXjTWuqDHFCBxH1Kjfti2kN1Yt6bJHDERiCucB9XcU2LDnovXk4AwVXAr4iCZtPgNvBG6rFT-W4WnUy7kfP-dNu5I5f1im4uug1N2djFKbThUw'
}


def send_custom_reward(profile_id):
    r1 = requests.post(
        url=SEND_REWARDS_URL,
        json={
            'description': '#action_new_year_title',
            'friend_name': "mr.Biggles",
            'is_hidden': 0,
            'monster_quantity': "1",
            'quantity': "20001",
            'reward_type': "MONSTER",
            'social_id': profile_id,
            'title': "#action_new_year_title",
        },
        headers=HEADERS,
    )
    r2 = requests.post(
        url=SEND_REWARDS_URL,
        json={
            'description': '#action_new_year_title',
            'friend_name': "mr.Biggles",
            'is_hidden': 0,
            'monster_quantity': "3",
            'quantity': "20000",
            'reward_type': "MONSTER",
            'social_id': profile_id,
            'title': "#action_new_year_title",
        },
        headers=HEADERS,
    )
    r3 = requests.post(
        url=SEND_REWARDS_URL,
        json={
            'description': "#action_new_year_title",
            'friend_name': "mr.Biggles",
            'is_hidden': 0,
            'quantity': "50",
            'reward_type': "STATUE_1",
            'social_id': profile_id,
            'title': "#action_new_year_title",
        },
        headers=HEADERS,
    )
    return r1.status_code, r2.status_code, r3.status_code


# print send_custom_reward('Kong9E673D44ADC83A8162848CFD87DC80D5')
query = client.query(kind='PlayerProfileV2')
query.add_filter('update_datetime', '>=', datetime.utcnow() - timedelta(days=2))
cursor, more, fetch_size = None, True, 250
profile_ids = set()
while more:
    query_iter = query.fetch(fetch_size, start_cursor=cursor)
    profiles = list(query_iter)
    cursor = query_iter.next_page_token
    more = len(profiles) == fetch_size

    print len(profiles)
    for profile in profiles:
        if profile.key.name in profile_ids:
            print 'AGAIN', profile.key.name
            continue
        else:
            profile_ids.add(profile.key.name)
        rv = send_custom_reward(profile.key.name)
        if rv != (200, 200, 200):
            print profile.key.name, rv