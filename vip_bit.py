
from datetime import datetime

from dateutil.parser import parse
from google.cloud import datastore


datastore_client = datastore.Client(project='blitztinyarmypanoramic')

query = datastore_client.query(kind='VipBit')
cursor, more, fetch_size = None, True, 150
count = 0
profile_ids = []
vip_bit = datastore_client.get(datastore_client.key("VipBit", "Steam76561198075929359"))
print vip_bit


def create_vip_reward(profile_id):
    reward = datastore.Entity(key=datastore_client.key("Reward", "vip_compensation_for_%s" % profile_id))
    reward['source'] = 'custom'
    reward['friend'] = None
    reward['reward_type'] = 'boost'
    reward['reward_id'] = 'vip_time'
    reward['count'] = 1000
    reward['extra'] = {
        'title': 'VIP Reward',
        'hint_title': 'compensation',
        'hint_description': 'wrong daily gold dust count',
    }
    reward['created_on'] = datetime.utcnow()
    reward['accepted'] = False
    reward['updated_on'] = False
    return reward


datastore_client.put_multi([create_vip_reward('Steam76561198072784093')])

# while more:
#     query_iter = query.fetch(fetch_size, start_cursor=cursor)
#     entities = list(query_iter)
#     cursor = query_iter.next_page_token
#     more = len(entities) == fetch_size
#     to_put = []
#     for vip_bit in entities:
#         try:
#             if vip_bit['VipPoints'] >= 150:
#                 continue
#         except:
#             pass
#         # try:
#         #     end_of_vip = parse(vip_bit['EndOfVip'])
#         #     if end_of_vip.replace(tzinfo=None) < datetime.utcnow().replace(tzinfo=None):
#         #         continue
#         # except:
#         #     pass
#         level = vip_bit.get('VipLvl')
#         if level is not None:
#             continue
#         print vip_bit
#         profile_ids.append(vip_bit.key.name)
#         vip_bit['VipLvl'] = 1
#         to_put.append(vip_bit)
#     datastore_client.put_multi(to_put)
#
# with open('vip_bits3.txt', 'w') as f:
#     f.write('\n'.join(profile_ids))
