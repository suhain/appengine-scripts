from operator import itemgetter

from google.cloud import datastore


datastore_client = datastore.Client(project='steamtinyarmypanoramic')

query = datastore_client.query(kind='PlayerLeagueStorage', ancestor=datastore_client.key('Tournament', '2017-10-25'))

participations = []


cursor, more, fetch_size = None, True, 250

def has_reward(profile_id):
    query = datastore_client.query(kind='Reward')
    query.add_filter()

while more:
    query_iter = query.fetch(fetch_size, start_cursor=cursor)
    entities = list(query_iter)
    cursor = query_iter.next_page_token
    more = len(entities) == fetch_size

    participations.extend([
        (e.key.name, e['points']) for e in entities if e['points'] > 0
    ])


for profile_id, points in sorted(participations, key=itemgetter(1), reverse=True):
    print profile_id, points

profile_ids = map(itemgetter(0), participations)

print len(profile_ids)
print ','.join(profile_ids[:100])
print ','.join(profile_ids[101:200])
print ','.join(profile_ids[201:300])
print ','.join(profile_ids[301:400])
print ','.join(profile_ids[401:500])
print ','.join(profile_ids[501:600])
print ','.join(profile_ids[601:700])
print ','.join(profile_ids[701:755])
