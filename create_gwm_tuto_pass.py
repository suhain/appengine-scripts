from google.cloud import datastore


client = datastore.Client(project='blitztinyarmypanoramic')

to_put = []
put_size = 100
with open('guild_profiles.txt', 'r') as f:
    for line in f.readlines():
        profile_id = line.strip()
        to_put.append(datastore.Entity(
            key=client.key('GuildWarMapTutorialPass', profile_id)
        ))

        if len(to_put) == put_size:
            client.put_multi(to_put)
            print len(to_put)
            to_put = []

    if len(to_put) > 0:
        client.put_multi(to_put)
        print len(to_put)
