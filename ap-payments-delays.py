# -*- coding:utf8 -*-
import json
import time
from google.cloud import logging
from dateutil import parser


logging_client = logging.Client(project='xxx-mightyparty', _use_grpc=False)

filter_ = """
resource.type="gae_app"
resource.labels.module_id="payments"
logName="projects/xxx-mightyparty/logs/appengine.googleapis.com%2Frequest_log"
protoPayload.resource="/gs_api/payments/epoch_init_txn" OR protoPayload.resource="/payments/callbacks/epoch/flexpost_callback"
protoPayload.method="POST"
timestamp>="2017-11-28T00:00:00.000000Z"
"""

init_time = {}
callback_time = {}
seconds_durations = []

for entry in logging_client.list_entries(filter_=filter_):
    time.sleep(1)
    timestamp = parser.parse(entry.timestamp.isoformat())

    if entry.payload['resource'] == u'/gs_api/payments/epoch_init_txn':
        try:
            datastore_id = None
            for line in entry.payload['line']:
                logMessage = line['logMessage']
                if logMessage.startswith('Response: '):
                    result = json.loads(logMessage.replace('Response: ', ''))
                    datastore_id = result['result']['datastore_id']
                    init_time[datastore_id] = timestamp.replace(tzinfo=None)
                    print "init", datastore_id, timestamp
        except Exception as e:
            print e
    if entry.payload['resource'] == u'/payments/callbacks/epoch/flexpost_callback':
        try:
            datastore_id = None
            captured = False
            for line in entry.payload['line']:
                logMessage = line['logMessage']
                if logMessage.startswith('Epoch ans: '):
                    captured = logMessage.replace('Epoch ans: ', '').startswith('Y')
                if logMessage.startswith('NCtxn id: '):
                    datastore_id = int(logMessage.replace('NCtxn id: ', ''))
            if datastore_id and captured:
                callback_time[datastore_id] = timestamp.replace(tzinfo=None)
                print "captured", datastore_id, timestamp
        except Exception as e:
            print e


for datastore_id, timestamp in init_time.items():
    if datastore_id in callback_time.keys():
        seconds_durations.append(
            (callback_time[datastore_id] - timestamp).total_seconds()
        )
print sum(seconds_durations) / len(seconds_durations) / 60

import pdb; pdb.set_trace()
