import csv
import json

from google.cloud import datastore


datastore_client = datastore.Client(project='xxx-mightyparty')

query = datastore_client.query(kind='PVPPlayer')
query.add_filter('league', '=', 10)


def load_monsters():
    with open('ap-monsters.csv', 'r') as f:
        monsters_reader = csv.DictReader(f)
        for monster in monsters_reader:
            yield monster


monsters = list(load_monsters())


def get_monster_rarity(monster_id):
    return next(
        (m['rarity'] for m in monsters if int(m['id']) == monster_id),
        'common'
    )


print monsters
print get_monster_rarity(1)
print get_monster_rarity(2)
print get_monster_rarity(3)
cursor, more, fetch_size = None, True, 250

while more:
    query_iter = query.fetch(fetch_size, start_cursor=cursor)
    pvp_players = list(query_iter)
    more = len(pvp_players) == fetch_size
    cursor = query_iter.next_page_token

    profile_ids = [pvp_player.key.name for pvp_player in pvp_players]
    army_bits = datastore_client.get_multi([
        datastore_client.key("ArmyBit", profile_id) for profile_id in profile_ids
    ])

    for army_bit in army_bits:
        try:
            collection = json.loads(army_bit['collection'])
            for m in collection:
                if get_monster_rarity(m['monster_id']) == 'legendary':
                    print army_bit.key.name
        except Exception as e:
            continue

