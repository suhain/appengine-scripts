from datetime import datetime
from google.cloud import datastore
account_ids = set()

# client = datastore.Client(project='steamtinyarmypanoramic')
client = datastore.Client(project='tinyarmypanoramic')
query = client.query(kind='PlayerProfileV2')
query.add_filter('update_datetime', '>', datetime(2017, 12, 20))
cursor, more, fetch_size = None, True, 500

while more:
    query_iter = query.fetch(fetch_size, start_cursor=cursor)
    profiles = list(query_iter)
    cursor = query_iter.next_page_token
    more = len(profiles) == fetch_size

    for profile in profiles:
        account_ids.add(profile.key.name)
    print len(account_ids)


with open('fog-players.txt', 'w') as f:
    cnt = 0
    for account_id in account_ids:
        f.write(account_id)
        if cnt > 0 and cnt % 25 == 0:
            f.write('\n')
        else:
            f.write(',')
        cnt += 1
