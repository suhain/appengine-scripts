SELECT
    t1.cell_x,
    t1.cell_y,
    t1.guild_id,
    CAST(t1.power * IFNULL(t2.boost_multiplier, 1) AS UNSIGNED)
FROM playerforce AS t1
LEFT OUTER JOIN powerboost AS t2 ON
    t1.cell_x = t2.cell_x AND
    t1.cell_y = t2.cell_y AND
    t1.guild_id = t2.guild_id AND
    t1.profile_id = t2.profile_id AND
    t1.war_id = t2.war_id AND
    t1.tick_number = t2.tick_number AND
    t1.room_number = t2.room_number
WHERE
    t1.war_id=3 AND t1.room_number=1 AND t1.tick_number=0
GROUP BY
    t1.cell_x AND t1.cell_y AND t1.guild_id
