from datetime import datetime, timedelta

from google.cloud import datastore


datastore_client = datastore.Client(project='xxx-mightyparty')
query = datastore_client.query(kind='PlayerProfile')
# since = datetime(2017, 11, 30, 0, 0, 0, 0).replace(tzinfo=None)
since = datetime.utcnow() - timedelta(days=7)
query.add_filter('created_datetime', '>=', since)
fetch_size, cursor, more = 100, None, True

new_players = 0
new_players_with_email_signup = 0
new_players_with_payments = 0
new_players_with_email_signup_with_payments = 0

while more:
    query_iter = query.fetch(fetch_size, start_cursor=cursor)
    _entities = list(query_iter)
    cursor = query_iter.next_page_token
    more = fetch_size == len(_entities)
    new_players += len(_entities)

    payments_info_bits = {
        bit.key.name: bit
        for bit in datastore_client.get_multi([
            datastore_client.key("PaymentsInfoBit", profile.key.name)
            for profile in _entities
        ])
    }

    for profile in _entities:
        # look for child_profiles
        has_payments = False
        has_email_login = False
        if profile.key.name in payments_info_bits.keys():
            payments_info_bit = payments_info_bits[profile.key.name]
            if payments_info_bit['PaymentsCount'] > 0:
                new_players_with_payments += 1
                has_payments = True

        child_profiles_query = datastore_client.query(kind='ParentProfile')
        child_profiles_query.add_filter('parent_id', '=', profile.key.name)

        for child_profile in child_profiles_query.fetch():
            if child_profile.key.name.startswith('EM'):
                has_email_login = True
                new_players_with_email_signup += 1
                break

        if has_payments and has_email_login:
            new_players_with_email_signup_with_payments += 1

    print new_players, new_players_with_email_signup, new_players_with_payments, new_players_with_email_signup_with_payments