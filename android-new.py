from collections import defaultdict
from datetime import datetime

import pytz
from google.cloud import datastore

datastore_client = datastore.Client(project='blitztinyarmypanoramic')
since = datetime(2018, 1, 18, tzinfo=pytz.UTC)
before = datetime(2018, 1, 19, tzinfo=pytz.UTC)
new = defaultdict(int)


def count_new(profile_ids):
    cnt = 0
    profiles = datastore_client.get_multi([datastore_client.key("PlayerProfile", profile_id) for profile_id in profile_ids])
    for profile in profiles:
        if profile['is_cheater'] or profile['is_qa_account']:
            continue
        if profile and since <= profile['created_datetime'] <= before:
            # print profile
            new[profile['created_datetime'].hour] += 1
    return cnt


profile_ids = []
with open('/home/hus/android-18.csv', 'r') as f:
    for line in f.readlines():
        profile_ids.append(line.strip())
        if len(profile_ids) == 200:
            count_new(profile_ids)
            profile_ids = []
for hour in sorted(new.keys()):
    print hour, new[hour]

print "total", sum(new.values())