from time import sleep

import requests

url = "http://xxx-mightyparty.appspot.com/_admin/guilds/war/end_tick"
war_id = 1708
room_numbers = 338
new_tick_number = 9


for room_number in range(1, room_numbers + 1):
    payload = "war_id=%s&new_tick_number=%s&room_number=%s" % (
        war_id, new_tick_number, room_number
    )
    headers = {
        'Content-Type': "application/x-www-form-urlencoded",
        'Cache-Control': "no-cache",
        'Postman-Token': "e9658199-c3c3-cde2-2816-61d9b462e999"
    }

    response = requests.request("POST", url, data=payload, headers=headers)

    print(response.text)
    sleep(1)
