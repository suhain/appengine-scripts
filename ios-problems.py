import time

from google.cloud import logging, datastore

logging_client = logging.Client(project='blitztinyarmypanoramic', _use_grpc=False)
datastore_client = datastore.Client(project='blitztinyarmypanoramic')


filter_ = """
resource.type="gae_app"
resource.labels.module_id="default"
resource.labels.version_id="production"
logName="projects/blitztinyarmypanoramic/logs/appengine.googleapis.com%2Frequest_log"
timestamp>="2017-09-15T00:00:00.000000Z"
"Pnk-Env: iPhonePlayer"
"""

player_ids = set()

for entry in logging_client.list_entries(filter_=filter_, page_size=250):
    time.sleep(5)

    try:
        lines = entry.payload['line']
        logMessage = lines[0]['logMessage']
        pnk_headers = logMessage.split('\n')
        profile_id = None
        login_id = None
        for pnk_header in pnk_headers:
            if pnk_header.startswith('Pnk-Player-Id'):
                header_name, header_value = map(lambda s: s.strip(), pnk_header.split(':'))
                profile_id = header_value
            if pnk_header.startswith('Pnk-Login-Id'):
                header_name, header_value = map(lambda s: s.strip(), pnk_header.split(':'))
                login_id = header_value

        if profile_id and profile_id not in player_ids:
            player_ids.add(profile_id)
            player_profile = datastore_client.get(datastore_client.key("PlayerProfile", profile_id))
            print "profile_id", profile_id, player_profile

        if login_id and login_id not in player_ids:
            player_ids.add(login_id)
            player_profile = datastore_client.get(datastore_client.key("PlayerProfile", login_id))
            print "login_id", login_id, player_profile

    except Exception as e:
        print e


print(len(player_ids))