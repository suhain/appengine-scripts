"""
Create backup:
    gcloud beta datastore export --kinds="Guild" --namespaces="(default)" gs://xxx-mightyparty.appspot.com/guilds_backup --project=xxx-mightyparty
    gcloud beta datastore export --kinds="Guild,GuildResources,GuildMember" --namespaces="(default)" gs://blitztinyarmypanoramic.appspot.com/guilds_backup --project=blitztinyarmypanoramic
Download backup:
    gsutil -m cp -R gs://xxx-mightyparty.appspot.com/guilds_backup .
    gsutil -m cp -R gs://blitztinyarmypanoramic.appspot.com/guilds_backup .
"""
import codecs
import json
import sys
import os
from collections import defaultdict
import logging
import csv

import random

import string

import requests

sys.path.append('/opt/google-cloud-sdk/platform/google_appengine')


from google.appengine.api.files import records
from google.appengine.datastore import entity_pb
from google.appengine.api import datastore

import psycopg2

created_rooms = []

logging.basicConfig(level=logging.WARNING, format='%(message)s')


def kind_iter(kind):
    base_dir = "data/kind_%s" % kind
    for filename in os.listdir(base_dir):

        with open("%s/%s" % (base_dir, filename), 'r') as f:
            reader = records.RecordsReader(f)
            for record in reader:
                entity_proto = entity_pb.EntityProto(contents=record)
                yield datastore.Entity.FromPb(entity_proto)


with psycopg2.connect(host="127.0.0.1", dbname="blitz", user="postgres") as conn:
# with psycopg2.connect(host="192.168.0.5", dbname="blitz", user="postgres") as conn:
    with conn.cursor() as cur:

        for entity in kind_iter('FriendLinks'):
            statement = "INSERT INTO friend_links(friend_from,friend_to,type,state,learner_rank,learner_league,auto_generated)" \
                        "VALUES (%s,%s,%s,%s,%s,%s,%s);"
            data = (
                entity['friend_from'].id_or_name(),
                entity['friend_to'].id_or_name(),
                entity['type'],
                entity['state'],
                entity.get('learner_rank'),
                entity.get('learner_league'),
                entity.get('auto_generated'),
            )
            cur.execute("BEGIN;")
            try:
                cur.execute(statement, data)
                cur.execute("COMMIT;")
            except psycopg2.IntegrityError:
                logging.warning(data)
                cur.execute("ROLLBACK;")
