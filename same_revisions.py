import time

from datetime import datetime, timedelta
from google.cloud import logging, datastore

logging_client = logging.Client(project='blitztinyarmypanoramic', _use_grpc=False)
datastore_client = datastore.Client(project='blitztinyarmypanoramic')


filter_ = """
resource.type="gae_app"
resource.labels.module_id="default"
resource.labels.version_id="production"
logName="projects/blitztinyarmypanoramic/logs/appengine.googleapis.com%2Frequest_log"
timestamp>="2017-11-23T00:00:00.000000Z"
"/gs_api/profile/commit"
"""

player_ids = set()

for entry in logging_client.list_entries(filter_=filter_, page_size=250):
    time.sleep(5)

    try:
        lines = entry.payload['line']
        for line in lines:
            logMessage = line['logMessage']
            if logMessage.startswith('Client / server revisions: '):
                s = logMessage.replace('Client / server revisions: ', '')
                s = s.strip()
                c_r, s_r = map(int, s.split('/'))
                if c_r == s_r:
                    print c_r, s_r

    except Exception as e:
        print e


print(len(player_ids))