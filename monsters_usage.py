import csv
from collections import defaultdict
from datetime import datetime, timedelta

import json
from google.cloud import datastore


client = datastore.Client(project='blitztinyarmypanoramic')

last_login_since = datetime.utcnow() - timedelta(days=30)
# last_login_since = datetime.utcnow().replace(
#     month=9,
#     day=4,
#     hour=0,
#     minute=0,
#     second=0,
#     microsecond=0,
# )
print last_login_since

query = client.query(kind='PlayerProfile')
query.add_filter('update_datetime', '>=', last_login_since)
# query.add_filter('is_cheater', '=', False)
query.add_filter('is_qa_account', '=', False)

cursor, more, fetch_size = None, True, 500


usage_data = defaultdict(lambda: defaultdict(int))
in_collection = defaultdict(int)
monsters_set = set()
total = defaultdict(int)
processed = set()
while more:
    query_iter = query.fetch(fetch_size, start_cursor=cursor)
    profiles = list(query_iter)
    cursor = query_iter.next_page_token
    more = len(profiles) == fetch_size

    profiles = [p for p in profiles if not p['is_cheater']]

    ids = [profile.key.name for profile in profiles]

    army_bits = client.get_multi(map(
        lambda pid: client.key("ArmyBit", pid),
        ids
    ))

    pvp_players = client.get_multi(map(
        lambda pid: client.key("PVPRoot", pid, "PVPPlayer", pid),
        ids
    ))

    leagues = {pvp_player.key.name: pvp_player['league'] for pvp_player in pvp_players}
    # print leagues

    for army_bit in army_bits:
        try:
            if army_bit.key.name in processed:
                continue
            league = leagues[army_bit.key.name]
            army = json.loads(army_bit['armies'])[army_bit['curArmy']]
            for m in army['deckPlaces']:
                usage_data[m['monster_id']][league] += 1
                monsters_set.add(m['monster_id'])
                total[m['monster_id']] += 1
            usage_data['players count'][league] += 1

            collection = json.loads(army_bit['collection'])
            for m in collection:
                in_collection[m['monster_id']] += 1
            processed.add(army_bit.key.name)
        except Exception as e:
            print e
            continue

    print "processed", len(processed)

leagues = list(reversed(list(range(31))))
with open('monsters_usage.csv', 'w') as f:
    writer = csv.writer(f)
    header = ['monster_id'] + leagues + ['collection', 'cur army']
    writer.writerow(header)
    monsters_set.add('players count')

    for monster_id in monsters_set:
        row = [monster_id]
        for l in leagues:
            row.append(usage_data[monster_id][l])
        row.append(in_collection[monster_id])
        row.append(total[monster_id])
        writer.writerow(row)
