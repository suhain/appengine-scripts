from datetime import datetime
from google.cloud import datastore

client = datastore.Client(project='steamtinyarmypanoramic')

# reward = client.get(client.key('PlayerProfileV2', 'Steam76561198032910281', 'PlayerReward', 2120001))
query = client.query(kind='PlayerReward', ancestor=client.key('PlayerProfileV2', 'Steam76561198032910281'))
# query.add_filter()

total = 0
for reward in query.fetch():
    try:
        if reward['reward_id'] == 'holiday_resource' and reward['create_date'].replace(tzinfo=None) >= datetime(2018, 2, 14):
            total += reward['quantity']
            print reward
    except KeyError:
        continue

print total
