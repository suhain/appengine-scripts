import requests


url = 'http://xxx-mightyparty.appspot.com/_admin/guilds/war/end_tick'

payload = {
    'war_id': 1707,
    'new_tick_number': 3,
}


for room_number in range(1, 327):
    payload['room_number'] = room_number
    response = requests.post(url, data=payload)
    print room_number, response
