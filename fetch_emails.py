from google.cloud import datastore

countries = [
    'CA', 'UK', 'NZ', 'AU', 'BY', 'MD', 'UA', 'RU', 'TJ', 'KZ', 'KG',
    'AZ', 'TM', 'UZ', 'GE'
]
# datastore_client = datastore.Client(project='blitztinyarmypanoramic')
#
# query = datastore_client.query(kind='PlayerSocialInfo')
#

# # query.add_filter('Country', '=', countries)
# fetch_size, cursor, more = 250, None, True
#
#
# emails = []
#
# while more:
#     query_iter = query.fetch(fetch_size, start_cursor=cursor)
#     _entities = list(query_iter)
#     cursor = query_iter.next_page_token
#     more = len(_entities) == fetch_size
#
#     entities = []
#     for e in _entities:
#         try:
#             if e['platform'] != 'ios' and e['email'] != '':
#                 entities.append(e)
#         except Exception as e:
#             print e
#
#     socials = {
#         e['profile_id']: e
#         for e in entities
#     }
#
#     name_bits = {
#         name_bit.key.name: name_bit
#         for name_bit in datastore_client.get_multi([
#             datastore_client.key("NameBit", e['profile_id'])
#             for e in entities
#         ])
#     }
#
#     for profile_id, name_bit in name_bits.items():
#         try:
#             if name_bit['Country'] in countries:
#                 email = socials[profile_id]['email']
#                 if email:
#                     emails.append(email)
#                     print email
#
#         except Exception as e:
#             print e
#
#
# with open('mightyparty_emails.txt', 'w') as f:
#     f.write('\n'.join(emails))


project = 'tinyarmypanoramic'
datastore_client = datastore.Client(project=project)
query = datastore_client.query(kind='PlayerEmails')
fetch_size, cursor, more = 250, None, True
emails = []
platforms = set()
while more:
    query_iter = query.fetch(fetch_size, start_cursor=cursor)
    _entities = list(query_iter)
    cursor = query_iter.next_page_token
    more = len(_entities) == fetch_size

    player_emails = {}

    for e in _entities:
        try:
            if e['game_platform'] != 'itunes':
                platforms.add(e['game_platform'])
                player_emails[e.key.name] = e
        except Exception as exc:
            print exc
    socials = {
        e.key.name: e
        for e in datastore_client.get_multi([
            datastore_client.key("PlayerSocial", ent.key.name)
            for ent in player_emails.values()
        ])
    }

    for profile_id, social in socials.items():
        try:
            if social['player_country'] in countries:
                emails.append(player_emails[profile_id]['email'])
        except Exception as exc:
            print exc

with open('%s_fog_emails.txt' % project, 'w') as f:
    f.write('\n'.join(emails))

print platforms