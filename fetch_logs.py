from datetime import datetime, timedelta

import os
from google.cloud import datastore, storage

BUCKET_NAME = 'blitz-client-logs'

datastore_client = datastore.Client(project='blitztinyarmypanoramic')
query = datastore_client.query(kind='ClientLogEntry')
query.add_filter('created', '>', datetime.utcnow() - timedelta(days=1))


cursor, more, fetch_size = None, True, 250

storage_client = storage.Client(project='blitztinyarmypanoramic')
bucket = storage_client.get_bucket('blitz-client-logs')


unique_profiles = set()


def get_log_entry_filename(log_entry):
    name = '%s_%s_%s' % (log_entry['profile_id'], log_entry['error'], log_entry['created'].replace(tzinfo=None))
    name = name.replace(" ", "")
    return name


while more:
    query_iter = query.fetch(fetch_size, start_cursor=cursor)
    log_entries = list(query_iter)
    more = len(log_entries) == fetch_size
    cursor = query_iter.next_page_token

    for entry in log_entries:
        unique_profiles.add(entry['profile_id'])
        try:
            filename = get_log_entry_filename(log_entry=entry)
            if os.path.exists('logs/%s' % filename):
                continue
            blob = bucket.get_blob(filename, client=storage_client)
            with open('logs/%s' % filename, 'w') as f:
                blob.download_to_file(f)
        except Exception as e:
            print e

print(len(unique_profiles))