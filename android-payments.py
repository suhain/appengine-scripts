from collections import defaultdict, Counter
from datetime import datetime

import pytz as pytz
from google.cloud import datastore

since = datetime(2017, 12, 20).replace(tzinfo=pytz.utc)
# before = datetime(2017, 12, 8).replace(tzinfo=pytz.utc)

datastore_client = datastore.Client(project='blitztinyarmypanoramic')
query = datastore_client.query(kind='InGamePurchase')
query.add_filter('created_on', '>=', since)

fetch_size, cursor, more = 250, None, True

new_android_ru_players = 0

data = defaultdict(list)

while more:
    query_iter = query.fetch(fetch_size, start_cursor=cursor)
    purchases = list(query_iter)
    more = fetch_size == len(purchases)
    cursor = query_iter.next_page_token
    purchases = [
        e for e in purchases
        if e['environment'] == 'android'
    ]

    for purchase in purchases:
        data[purchase['created_on'].date()].append(purchase)

for date, purchases in data.items():
    print date, len(purchases)
    counter = Counter()
    for purchase in purchases:
        counter[purchase['lot_id']] += 1
    print counter
