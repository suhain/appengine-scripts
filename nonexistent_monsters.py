import csv
import json

from google.cloud import datastore

monster_ids = set()
with open('Monsters.csv', 'r') as f:
    reader = csv.DictReader(f)
    for monster_data in reader:
        monster_ids.add(int(monster_data['id']))

print "total monsters", len(monster_ids)


datastore_client = datastore.Client(project='blitztinyarmypanoramic')
query = datastore_client.query(kind='ArmyBit')

more, cursor, fetch_size = True, None, 250

while more:
    query_iter = query.fetch(fetch_size, start_cursor=cursor)
    entities = list(query_iter)
    cursor = query_iter.next_page_token
    more = len(entities) == fetch_size

    for army_bit in entities:
        try:
            collection = json.loads(army_bit['collection'])

            for monster_data in collection:
                if monster_data['monster_id'] not in monster_ids:
                    print monster_data['monster_id'], army_bit
        except:
            continue