import json

import requests

url = "http://dev.blitztinyarmypanoramic.appspot.com/gs_api/guilds/get_members"

payload = {"members": True, "requests": False,  "recruits": False, "invites": False}


headers = {
    'pnk-env': "WebGLPlayer",
    'pnk-version': "skip",
    'pnk-device-id': "postman",
    'pnk-player-id': "test_1707535",
    'pnk-secure-string': "skip",
    'pnk-request-client-start-time': "1",
    'pnk-number': "1",
    'content-type': "application/json",
    'cache-control': "no-cache",
    'postman-token': "291d8b77-70e3-bab1-65cf-89af9be36412"
    }

response = requests.request("POST", url, data=json.dumps(payload), headers=headers)

data = json.loads(response.text)
print(data)
member_ids = [m['profile_id'] for m in data['result']['members']]

print(member_ids)

url = "http://dev.blitztinyarmypanoramic.appspot.com/gs_api/guild_wars/send_force"


for m in member_ids:
    for monster_id in range(2, 5):
        payload = {"x": 0, "y": 3, "monster_ids": [monster_id]}
        headers = {
            'pnk-env': "WebGLPlayer",
            'pnk-version': "skip",
            'pnk-device-id': "postman",
            'pnk-login-id': m,
            'pnk-secure-string': "skip",
            'pnk-request-client-start-time': "1",
            'pnk-number': "1",
            'content-type': "application/json",
            'cache-control': "no-cache",
            'postman-token': "30f4ea4f-43ee-096a-d573-624dca84783a"
            }

        response = requests.request("POST", url, data=json.dumps(payload), headers=headers)

        print(response.text)