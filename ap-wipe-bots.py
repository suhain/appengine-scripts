from datetime import datetime

import requests
from google.cloud import datastore

wipe_url = "http://dev.xxx-mightyparty.appspot.com/_admin/profiles/%s?full_wipe=0"
datastore_client = datastore.Client(project='xxx-mightyparty')
query = datastore_client.query(kind='PlayerProfile')
query.add_filter('created_datetime', '<', datetime(year=2017, month=8, day=1))

fetch_size, more, cursor = 250, True, None

while more:
    query_iter = query.fetch(fetch_size, start_cursor=cursor)
    profiles = list(query_iter)
    more = fetch_size == len(profiles)

    pvp_players = datastore_client.get_multi([
        datastore_client.key("PVPRoot", p.key.name, "PVPPlayer", p.key.name)
        for p in profiles
    ])
    for profile in profiles:
        try:
            pvp_player = next(
                p for p in pvp_players if p.key.name == profile.key.name
            )
        except StopIteration:
            continue
        league = pvp_player['league']
        if league < 6:
            continue

        # print(profile.key.name, league)

        response = requests.delete(wipe_url % profile.key.name, headers={
            'Cookie': 'ACSID=~AJKiYcGv9e7IpFM76IH-alBFHmd85Lj9njz1sDSwh4o7KJZSY8Xc1a5wUfvrSM0hFjkgogeb65bQ2cV4O8gvkXQfnLOxwCu22pC4i_ZMxKNYo_BrjIMbxj8ySPiM-nk2BR_7VwoMO7wea2ySuGHCXMvC57HNjJaXdPIH7kwKAlUvYOX1314Epp65bTWumsQUAs_YuVZg00Z85UEaNr-UP8cAwUPtaGXq02AqYM3RZZpLg__z2R1hEYT2ImhY33WLnV_B4IppHhVDtvbcNExlJQmH_DD0jDWj6GuMbW4AMaBy4BNFuzh-OSS6PLaF2NrUJOSat9l3D9Vw'
        })
        print(profile.key.name, response)
