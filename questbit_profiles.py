from urlparse import parse_qsl

from datetime import datetime, timedelta
from google.cloud import datastore


datastore_client = datastore.Client(project='blitztinyarmypanoramic')
query = datastore_client.query(kind='QuestBit')

more, cursor, fetch_size = True, None, 250
result = []

while more:
    query_iter = query.fetch(fetch_size, start_cursor=cursor)
    entities = list(query_iter)
    more = len(entities) == fetch_size
    cursor = query_iter.next_page_token

    profile_ids = []
    for quest_bit in entities:
        try:
            if quest_bit['lastEventEnterId'] != 46:
                profile_ids.append(quest_bit.key.name)
        except:
            continue

    profiles = filter(bool, datastore_client.get_multi([
        datastore_client.key(profile_id) for profile_id in profile_ids
    ]))

    for profile in profiles:
        if profile.update_datetime >= datetime.utcnow() - timedelta(hours=10):
            print profile.key.name

