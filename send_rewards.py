import requests
from datetime import datetime, timedelta
from google.cloud import datastore

datastore_client = datastore.Client(project='blitztinyarmypanoramic')
query = datastore_client.query(kind='PlayerProfile')
query.add_filter('update_datetime', '>=', datetime.utcnow() - timedelta(days=2))


def send_custom_reward(profile_ids, count=1, source='custom', reward_id='vip_time', reward_type='boost', extra=None):
    request_data = {
        'reward': {
            'autoclaim': False,
            'count': count,
            'source': source,
            'reward_id': reward_id,
            'type': reward_type,
            'extra': extra or {},
        },
        'to': profile_ids,
        'to_type': 'player_ids',
    }
    response = requests.post(
        url='http://dev.blitztinyarmypanoramic.appspot.com/_admin/rewards/send',
        json=request_data,
        headers={
            'COOKIE': "ACSID=~AJKiYcF2qd7bJWCYuQyFH8-Fp5-6r_y3ld7MxCB-GElgztMHZAFEjUvpxTSk57MCo2_iFAoxaJ8DkCSzR9anYzMsdEUGtchukRdd2Oli1epfjKLfWYxlm0A-Gj-UP8TgrikXe6OD0Kkysx2TPP1JZinJV8mAUZ6zHF_jkuU0FEorkiQJePncdNWkCd2c_Xbk2L_YgEz0bpPPk7J67G-6p45w7IP71A3OTw7Ila4LZM0aDUhFX48aRed7USeRKTTBSYmkAu5hHLmymrQqwf5GFRrfMQgvNVkQnRgn9VEVDbK8c3Za4AxQnb3cDyIL5x3g52TPk1yxUxod_5f6Rsq7Ne2WA-t93ZNJDA"
        }
    )
    return response


print send_custom_reward(['Fb28882f0197031ee6213dc321f9fd3581'], extra={
    'title': 'Merry \nChristmas!',
    'hint_title': 'Merry \nChristmas!',
    'hint_description': 'Merry \nChristmas!',
}, count=86400).json()


cursor, more, fetch_size = None, True, 250

while more:
    query_iter = query.fetch(fetch_size, start_cursor=cursor)
    profiles = list(query_iter)
    cursor = query_iter.next_page_token
    more = len(profiles) == fetch_size

    vip_bits = datastore_client.get_multi([
        datastore_client.key("VipBit", profile.key.name)
        for profile in profiles
    ])

    to_send = []
    for vip_bit in vip_bits:
        try:
            if vip_bit['VipPoints'] > 0:
                to_send.append(vip_bit.key.name)
        except:
            continue

    try:
        print to_send
        print send_custom_reward(
            to_send, extra={
                'title': 'Merry \nChristmas!',
                'hint_title': 'Merry \nChristmas!',
                'hint_description': 'Merry \nChristmas!',
            }, count=86400
        ).json()
    except Exception as e:
        print e

