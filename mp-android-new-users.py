import random
import time

from datetime import datetime

import pytz
from google.cloud import logging, datastore


logging_client = logging.Client(project='blitztinyarmypanoramic', _use_grpc=False)
datastore_client = datastore.Client(project='blitztinyarmypanoramic')
since = datetime(2017, 12, 20, tzinfo=pytz.UTC)
before = datetime(2017, 12, 21, tzinfo=pytz.UTC)

filter_ = """
resource.type="gae_app"
resource.labels.module_id="default"
resource.labels.version_id="production"
logName="projects/blitztinyarmypanoramic/logs/appengine.googleapis.com%2Frequest_log"
timestamp>="{since}"
timestamp<="{before}"
protoPayload.resource="/gs_api/profile/commit"
"Pnk-Env: Android"
""".format(
    since=since.isoformat(), before=before.isoformat()
)
print filter_


def parse_pnk_headers(logMessage):
    headers = {}
    for header in logMessage.split('\n'):
        header_name, header_value = header.split(': ')
        header_name = header_name.strip()
        header_value = header_value.strip()
        headers[header_name] = header_value
    return headers


def is_new_android_user(profile_id):
    profile = datastore_client.get(datastore_client.key("PlayerProfile", profile_id))
    if profile and since <= profile['created_datetime'] <= before:
        return True
    return False


android_users = set()
new_users_count = 0
for entry in logging_client.list_entries(filter_=filter_):
    try:
        lines = entry.payload['line']
        for line in lines:
            logMessage = line['logMessage']
            try:
                headers = parse_pnk_headers(logMessage)
                if headers['Pnk-Player-Id'] not in android_users and is_new_android_user(headers['Pnk-Player-Id']):
                    new_users_count += 1
                android_users.add(headers['Pnk-Player-Id'])
            except (KeyError, ValueError):
                pass
            except Exception as e:
                print type(e), e
            else:
                continue
    except:
        continue
    if random.randint(0, 4) == 0:
        time.sleep(random.randint(0, 1))
    print len(android_users), new_users_count