from urlparse import parse_qsl

from google.cloud import datastore


datastore_client = datastore.Client(project='blitztinyarmypanoramic')
query = datastore_client.query(kind='Reward')
query.add_filter('source', '=', 'guild_war')

more, cursor, fetch_size = True, None, 250

while more:
    query_iter = query.fetch(fetch_size, start_cursor=cursor)
    entities = list(query_iter)
    more = len(entities) == fetch_size
    cursor = query_iter.next_page_token

    to_delete = []

    for e in entities:
        try:

            params = dict(parse_qsl(e.key.name))
            if params['war_id'] == '144':
                to_delete.append(e.key)
        except Exception as exc:
            print exc

    if to_delete:
        print to_delete
        datastore_client.delete_multi(to_delete)
