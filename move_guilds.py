"""
Create backup:
    gcloud beta datastore export --kinds="Guild" --namespaces="(default)" gs://xxx-mightyparty.appspot.com/guilds_backup --project=xxx-mightyparty
    gcloud beta datastore export --kinds="Guild,GuildResources,GuildMember" --namespaces="(default)" gs://blitztinyarmypanoramic.appspot.com/guilds_backup --project=blitztinyarmypanoramic
Download backup:
    gsutil -m cp -R gs://xxx-mightyparty.appspot.com/guilds_backup .
    gsutil -m cp -R gs://blitztinyarmypanoramic.appspot.com/guilds_backup .
"""
import codecs
import json
import sys
import os
from collections import defaultdict
import logging
import csv

import random

import string

import requests

sys.path.append('/opt/google-cloud-sdk/platform/google_appengine')


from google.appengine.api.files import records
from google.appengine.datastore import entity_pb
from google.appengine.api import datastore

import psycopg2

created_rooms = []

logging.basicConfig(level=logging.WARNING, format='%(message)s')
current_war_id = 69
current_tick_number = 11
guild_ids_map = {}
old_participations = {}
old_ratings = {}


class RoomCreated(Exception):
    pass


class ResponseError(Exception):
    pass


def create_war_map(cursor, old_member_id, room_number):
    if room_number in created_rooms:
        raise RoomCreated()
    response = requests.post(
        url='http://dev.blitztinyarmypanoramic.appspot.com/gs_api/guild_wars/get_map',
        data=json.dumps({
            'tick_number': -1
        }),
        headers={
            'Pnk-Device-Id': 'postman',
            'Pnk-Player-Id': old_member_id,
            'Pnk-Login-Id': old_member_id,
            'Pnk-Env': 'WindowsPlayer',
            'Pnk-Version': 'skip',
            'Pnk-Secure-String': 'skip',
            'PNK-Request-Client-Start-Time': 'skip',
            'Pnk-Number': '1',
            'Content-Type': 'application/json',
        }
    )
    response_data = response.json()
    result = response_data['result']
    error = response_data['error']
    if error:
        raise ResponseError(json.dumps(error))
    else:
        for cell in result['cells']:
            x = cell['x']
            y = cell['y']
            base_id = cell['base_id']
            guild_id = guild_ids_map.get(cell['guild_id'])
            landmark_id = cell['landmark_id']
            if landmark_id == -1:
                landmark_id = None
            power = cell['power']
            ticks_under_control = cell['ticks_under_control']

            statement = "INSERT INTO cell_state(war_id,cell_x,cell_y,tick_number,room_number,guild_id,power,base_id,landmark_id,under_control)" \
                        "VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
            data = (
                current_war_id, x, y, current_tick_number, room_number, guild_id, power, base_id, landmark_id, ticks_under_control
            )
            cursor.execute(statement, data)


with codecs.open('participations.csv', encoding='utf-8-sig') as f:
    for r in csv.DictReader(f):
        old_participations[int(r['guild_id'])] = {
            'in_room_place': int(r['in_room_place']),
            'room_group_number': int(r['room_group_number']),
            'room_number': 1000 + int(r['room_number']),
            'room_first_tick_number': current_tick_number,
        }


with codecs.open('rating.csv', encoding='utf-8-sig') as f:
    for r in csv.DictReader(f):
        old_ratings[int(r['guild_id'])] = int(r['points'])

print old_participations
print old_ratings


def kind_iter(kind):
    base_dir = "profilebackdata/kind_%s" % kind
    for filename in os.listdir(base_dir):

        with open("%s/%s" % (base_dir, filename), 'r') as f:
            reader = records.RecordsReader(f)
            for record in reader:
                entity_proto = entity_pb.EntityProto(contents=record)
                yield datastore.Entity.FromPb(entity_proto)


influence = defaultdict(int)
for entity in kind_iter('GuildResources'):
    influence[entity.key().id_or_name()] = entity['influence']

members = defaultdict(list)


for entity in kind_iter('GuildMember'):
    key = entity.key()
    try:
        member_data = {
            'profile_id': key.id_or_name(),
            'guild_id': entity['guild_id'],
            'joined_on': entity['joined_on'],
            'role': entity.get('roles', ['member'])[0],
        }
        members[entity['guild_id']].append(member_data)
    except:
        logging.exception('error occurred while parsing member data')


shop_data = {}
for entity in kind_iter('GuildShop'):
    key = entity.key()
    try:
        data = (
            entity['unlocked_item_ids'],
            entity['unlocked_item_levels'],
        )
        shop_data[key.id_or_name()] = data
    except:
        logging.exception('error while read shop data')


def insert_guild(cursor, name, slogan, rank, icon, created_on, is_qa_guild,
                 min_might, influence, auto_accept_requests, internal_message):
    statement = "INSERT INTO guild (id,name,slogan,rank,icon,created_on," \
                "is_qa_guild,min_might,influence,auto_accept_requests," \
                "internal_message) VALUES (nextval('guilds_id_seq'),%s,%s,%s," \
                "%s,%s,%s,%s,%s,%s,%s) RETURNING id;"
    data = (
        name, slogan, rank, icon, created_on,
        'true' if is_qa_guild else 'false',
        min_might, influence,
        'true' if auto_accept_requests else 'false', internal_message
    )
    cursor.execute(statement, data)
    result = cursor.fetchone()[0]
    return result


def insert_guild_fame_rating(cursor, guild_id):
    statement = "INSERT INTO guild_fame_rating (guild_id,summary_fame) " \
                "VALUES (%s,%s);"
    data = (guild_id, 0)
    cursor.execute(statement, data)


def insert_member(cursor, profile_id, guild_id, joined_on, role):
    statement = "INSERT INTO guild_member(profile_id,guild_id,joined_on,role)" \
                "VALUES (%s,%s,%s,%s);"
    data = (profile_id, guild_id, joined_on, role)
    cursor.execute(statement, data)


def insert_guild_war_data(cursor, new_guild_id, old_guild_id):
    statement = "INSERT INTO guild_war_participation (guild_id,war_id," \
                "room_number,room_group_number,in_room_place," \
                "room_first_tick_number) " \
                "VALUES (%s,%s,%s,%s,%s,%s);"
    data = (
        new_guild_id, current_war_id,
        old_participations[old_guild_id]['room_number'],
        old_participations[old_guild_id]['room_group_number'],
        old_participations[old_guild_id]['in_room_place'],
        old_participations[old_guild_id]['room_first_tick_number'],
    )
    cursor.execute(statement, data)
    statement = "INSERT INTO guild_war_rating(war_id,guild_id,points)" \
                "VALUES (%s,%s,%s)"
    data = (current_war_id,new_guild_id, old_ratings.get(old_guild_id, 0))
    cursor.execute(statement, data)


def insert_shop_data(cursor, new_guild_id, old_guild_id):
    ids, levels = shop_data.get(old_guild_id, [(), ()])
    for id, level in zip(ids, levels):
        cursor.execute("SAVEPOINT sh%s;" % id)
        try:
            cursor.execute(
                "INSERT INTO guild_shop_item(guild_id,item_id,level,updated_on)"
                "VALUES (%s,%s,%s,now())",
                (new_guild_id, id, level)
            )
            cursor.execute("RELEASE SAVEPOINT sh%s;" % id)
        except psycopg2.IntegrityError:
            logging.exception('')
            cursor.execute("ROLLBACK TO SAVEPOINT sh%s;" % id)


# with psycopg2.connect(host="127.0.0.1", dbname="blitz", user="postgres") as conn:
with psycopg2.connect(host="192.168.0.5", dbname="blitz", user="postgres") as conn:
    with conn.cursor() as cur:

        # for entity in kind_iter('PlayerProfile'):
        #     cur.execute("BEGIN;")
        #     try:
        #         statement = "INSERT INTO player_profile(profile_id,revision,created_datetime,update_datetime,pvp_active,use_in_pvp,is_cheater,is_qa_account,is_paying)" \
        #                     "VALUES ('%s',1,now(),now(),true,true,false,false,false)" % entity.key().id_or_name()
        #         cur.execute(statement)
        #         cur.execute("COMMIT;")
        #     except psycopg2.IntegrityError:
        #         cur.execute("ROLLBACK;")
        #         continue

        for entity in kind_iter('Guild'):
            old_guild_id = entity.key().id_or_name()
            guild_data = dict(
                name=entity['name'][:40],
                slogan=entity['slogan'][:40],
                rank=entity['rank'],
                icon=entity['icon'],
                created_on=entity['created_on'],
                is_qa_guild=entity.get('is_qa_guild'),
                min_might=entity['min_might'],
                influence=influence[old_guild_id],
                auto_accept_requests=entity.get('auto_accept_requests'),
                internal_message=(entity.get('internal_message') or ''),
            )
            logging.debug(guild_data)
            guild_inserted = False
            for try_num in range(5):
                cur.execute("BEGIN;")
                try:
                    # insert guild
                    new_guild_id = insert_guild(cur, **guild_data)

                    # insert members
                    members_inserted = 0
                    for i, member_data in enumerate(members[old_guild_id]):
                        member_data['guild_id'] = new_guild_id
                        cur.execute("SAVEPOINT s%s;" % i)
                        try:
                            insert_member(cur, **member_data)
                            cur.execute("RELEASE SAVEPOINT s%s;" % i)
                            members_inserted += 1
                        except psycopg2.IntegrityError:
                            logging.exception('error while inserting member_data')
                            cur.execute("ROLLBACK TO SAVEPOINT s%s;" % i)

                    insert_guild_fame_rating(cur, new_guild_id)
                    if old_guild_id in old_participations.keys():
                        cur.execute("SAVEPOINT participation;")
                        try:
                            insert_guild_war_data(cur, new_guild_id, old_guild_id)
                            cur.execute("RELEASE SAVEPOINT participation;")
                        except psycopg2.IntegrityError:
                            logging.exception('')
                            cur.execute("ROLLBACK TO SAVEPOINT participation;")

                    if old_guild_id in shop_data.keys():
                        insert_shop_data(cur, new_guild_id, old_guild_id)

                    if members_inserted > 0:
                        cur.execute("COMMIT;")
                        guild_inserted = True
                        break
                    else:
                        cur.execute("ROLLBACK;")
                        break

                except psycopg2.IntegrityError:
                    cur.execute("ROLLBACK;")
                    guild_data['name'] += random.choice(string.ascii_letters)
                    logging.info("retrying...", exc_info=1)

            if not guild_inserted:
                # logging.warning('not inserted %s' % guild_data)
                continue
            else:
                guild_ids_map[old_guild_id] = new_guild_id

        for old_guild_id, participation in old_participations.items():
            room_number = participation['room_number']

            for member_data in members[old_guild_id]:
                member_id = member_data['profile_id']
                cur.execute("BEGIN;")
                try:
                    create_war_map(cur, member_id, participation['room_number'])
                    cur.execute("COMMIT;")
                    created_rooms.append(participation['room_number'])
                    break
                except RoomCreated:
                    cur.execute("ROLLBACK;")
                    continue
                except Exception:
                    cur.execute("ROLLBACK;")
                    logging.exception('')
